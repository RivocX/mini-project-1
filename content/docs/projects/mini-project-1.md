+++
title = "Mini Project 1"
description = "Create a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = 'Create a static site with <a href="https://www.getzola.org/">Zola</a>, a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization.'
toc = true
top = false
+++

## Requirements

Static site generated with Zola

Home page and portfolio project page templates

Styled with CSS

GitLab repo with source code

## Result

Link to live static site: [Site →](https://rivocx.gitlab.io/mini-project-1)

Link to GitLab repo: [Repo →](https://gitlab.com/RivocX/mini-project-1.git)


## Help

Get help on Doks. [Help →](../../help/faq/)
