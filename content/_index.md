+++
title = "IDS 721 Portfolio"


# The homepage contents
[extra]
lead = 'This website is the portfolio work of <b>Puyang Xu</b>'
url = "/docs/projects/mini-project-1/"
url_button = "Get started"
repo_version = "GitLab"
repo_license = "Duke University, Spring 2024. Repository: "
repo_url = "https://gitlab.com/RivocX/mini-project-1.git"

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/projects/mini-project-1/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

+++
