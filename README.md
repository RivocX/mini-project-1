# Mini Project 1
#### Puyang Xu
Link to live static site: [Site →](https://rivocx.gitlab.io/mini-project-1)

Link to GitLab repo: [Repo →](https://gitlab.com/RivocX/mini-project-1.git)

## Screenshot of the website:
![Screenshot](./Website.png)
